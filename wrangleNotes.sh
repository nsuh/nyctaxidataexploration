cut -d "," -f 9 first.csv
cut -d "," -f 9 first.csv |  awk '{total = total + $1}END{print total}' 
^30 secs for 1 file
cut -d "," -f 9 time.csv //gets 9th column of time - trip time.
awk -F ',' '{printf"%g\n", $11-$10}' fare.csv  //11th column - 10th column - total minus tolls.

awk -F ',' '{printf"%g\n", $11-$10}' fare.csv | sed 1d > totalMinusToll.csv
#sed 1d gets rid of the first row, which is junk.


#calculating total fee minus tolls from trip_fare files.
#takes 11th column (fee) minus 10th column (tolls), cuts off the first line (which is always 0)
#saves in out_file name
#works so far!!
for f in *.csv
	do
		awk -F ',' '{printf"%g\n", $11-$10}' $f | sed 1d > out_${f}
	done


#!/bin/bash
for f in `ls *.csv | sort -n -t _ -k 3`
	do
		awk -F ',' '{printf"%g\n", $11-$10}' $f | sed 1d > out_${f}
	done
	
#cat file1 file2 file3 file4 file5 file6 > out.txt
