# Databricks notebook source exported at Tue, 2 Jun 2015 21:03:09 UTC
from pyspark.mllib.regression import LabeledPoint, LinearRegressionWithSGD, LinearModel
from numpy import array


# COMMAND ----------

import urllib
ACCESS_KEY = "..."
SECRET_KEY = "..."
ENCODED_SECRET_KEY = urllib.quote(SECRET_KEY)
BUCKET = "nytaxidata"
MOUNT_NAME = "myMountName"

myRDD = sc.textFile("s3n://%s:%s@%s/data.csv" % (ACCESS_KEY, SECRET_KEY, BUCKET))


#myRDD.take(5)

# COMMAND ----------

def parsePoint(line):
    values = [float(x) for x in line.replace(',', ' ').split(' ')]
    return LabeledPoint(values[1], values[0:1])

taxi = myRDD.map(parsePoint)

taxi.take(5)

# COMMAND ----------


model = LinearRegressionWithSGD.train(taxi, iterations = 30)

# COMMAND ----------

model


# COMMAND ----------


