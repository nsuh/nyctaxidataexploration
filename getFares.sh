#!/bin/bash
for f in `ls *.csv | sort -n -t _ -k 3`
	do
		awk -F ',' '{printf"%g\n", $11-$10}' $f | sed 1d > out_${f}
	done

cat `find . -name 'out_*' | sort -n -t _ -k 3` > allFares.csv